package com.adalid;

import com.adalid.ejercicios.Ejercicios;

/*
 * Operaciones C.R.U.D.:
 * Create, Read, Update y Delete sobre una entidad: 
 * Una entidad ser�a la parte programada de un concepto que queramos gestionar:
 * Usuarios, Producto, Empresa. */
public class Programa {

	public static void main(String[] args) {
		// Invocamos a un m�todo est�tico, poniendo la clase . metodo()
		// EjemploUsuarios.crearYMostrarUsusarios();
		Ejercicios.ejercicio2();
		EjemploLibreriaJSON.cargarUsuarios();
	}
}
