package com.adalid.modelo;

import java.util.ArrayList;

/* Operaciones CRUD sobre una lista de usuarios.
 * Create, Read, Update, Delete */

public class GestionarUsuarios {
	private ArrayList<Usuario>  listaUsuarios = null;

	public GestionarUsuarios() {
		super();
		listaUsuarios = new ArrayList<Usuario>();
	}
	public void create(String nombre, int edad, char genero) {
		Usuario usuario = new Usuario(nombre, "", edad, genero);
		listaUsuarios.add(usuario);
	}
	public void mostrarTodos() {
		for (Usuario usuario : listaUsuarios) {
			usuario.mostrarDatos();
		}
	}
	public void mostrarUnoPorNombre(String nombreABuscar) {
		for (Usuario usuario : listaUsuarios) {
			if (usuario.nombre.equalsIgnoreCase(nombreABuscar)) {
				System.out.println("Encontrado: ");
				usuario.mostrarDatos();
				return;
			}
		}
		System.out.println("No se ha encontrado " + nombreABuscar);
	}
	public void borrarUnoPorNombre(String nombreABuscar) {
		Usuario usuarioBorrar = null;
		for (Usuario usuario : listaUsuarios) {
			if (usuario.nombre.equalsIgnoreCase(nombreABuscar)) {
				System.out.println("Eliminar: ");
				usuario.mostrarDatos();
				usuarioBorrar = usuario;
				break;
			}
		}
		if (usuarioBorrar == null) {
			System.out.println("No se ha encontrado " + nombreABuscar);			
		}
		else {
			this.listaUsuarios.remove(usuarioBorrar);
		}
	}
	
}
