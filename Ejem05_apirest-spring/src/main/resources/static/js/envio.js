/* Las clases son también ES6 ES2015 
En general, las funciones no se pueden sobrecargar:
 Para usar varios parámetros usamos arguments
*/
class EnvioUsuario {
    
    constructor(traerUsuarios) {
    	this.traerUsuarios = traerUsuarios;
        // this es obligatorio en JS
        this.form = document.getElementById("form-usuario");
        this.inputNombre = document.getElementById("input-nombre");
        this.inputEstado = document.getElementById("input-estado");
        // Como el formulario es el que lanza el evento, se apropia de la palabra this dentro del método. Así que tenemos que "engañarle" con bind, para que this sea realmente el this de nuestra clase
        this.form.onsubmit = this.alIntentarEnviar.bind(this);
    }
    // Los métodos se declaran así:
    alIntentarEnviar(evento) {
        // Cancelamos el evento por defecto de envio de formulario
        evento.preventDefault();
        let usuario = {
            "nombre": this.inputNombre.value,
            "estado": this.inputEstado.checked ? true : false   /* Usamos el operador ternario, que es como un if () else chiquitito, en expresión */
        };
        alert(JSON.stringify(usuario));
        let traerUsuarios = this.traerUsuarios;
        fetch("http://localhost:8080/api/usuarios", {
            "method": "POST",
            headers: {
                "Content-type": "application/json"
            } ,
            body: JSON.stringify(usuario)
        }).then(function(respuestaHTTP)  {
            return respuestaHTTP.json();
        }).then(function(boolean) {
            console.log(boolean);
            alert(boolean);
            traerUsuarios();
        })
        .catch(function() {
            alert("Error  al crear!");
        });
        window.localStorage.setItem("token-api", token);
    }
};