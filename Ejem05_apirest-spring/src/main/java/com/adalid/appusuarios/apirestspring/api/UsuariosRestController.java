package com.adalid.appusuarios.apirestspring.api;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.*;

import com.adalid.appusuarios.modelo.GestionUsuariosBD;
import com.adalid.appusuarios.modelo.Usuario;

@RestController
@RequestMapping("/api/usuarios")
@CrossOrigin
public class UsuariosRestController {

	GestionUsuariosBD gesUsu;
	
	public UsuariosRestController() throws ClassNotFoundException {
		gesUsu = new GestionUsuariosBD();
	}
	
	// Recepcionamos el método HTTP, POST, para crear un nuevo usuario.
	@PostMapping
	public boolean nuevoUsuario(@RequestBody Usuario usuario) {
		gesUsu.create(usuario.nombre, usuario.edad, usuario.genero);
		System.out.println("Crear usuario " + usuario.nombre);
		return true;
	}
	@GetMapping
	public ArrayList<Usuario> leerTodos() {
		return gesUsu.mostrarTodos();
	}
	@DeleteMapping("/{id}")
	public boolean borrarPorNombre(@PathVariable int id) {
		gesUsu.borrarUnoPorID(id);
		return true;
	}
}
