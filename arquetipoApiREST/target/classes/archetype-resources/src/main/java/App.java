package $com.adalid;

/**
 * Servicio ${nombreServicio}
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Servicio basico: ${nombreServicio}"  );
		String ${nombreServicio} = "${nombreServicio}";
        System.out.println( "Iniciando ${nombreServicio}..."  );
    }
}
